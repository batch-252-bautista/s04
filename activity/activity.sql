-- A. Find all artists that has letter d in its name
SELECT * FROM artists WHERE name LIKE "%d%";


-- B. Find all songs that has a length of less than 230
SELECT * FROM songs WHERE length < 230;


-- C. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length)
SELECT albums.album_title, songs.song_name, songs.length from albums
	JOIN songs ON albums.id = songs.album_id;


-- D. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	WHERE artists.name LIKE "%a%";


-- E. Sort the albums in Z-A order. (Show only the first 4 records)
SELECT * FROM albums ORDER BY album_title DESC;

-- used LIMIT to show only first 4 records
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;


-- F. Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id
	ORDER BY albums.album_title DESC;